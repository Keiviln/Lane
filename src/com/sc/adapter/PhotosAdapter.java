package com.sc.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sc.db.DBStorage;
import com.sc.db.MarkRecord;
import com.sc.lane.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class PhotosAdapter extends BaseAdapter{
	private LayoutInflater mInflater;
	private Activity mActivity;
	private List<MarkRecord> mRecords;
	
	public PhotosAdapter(Activity activity){
		this.mActivity = activity;
		this.mInflater = LayoutInflater.from(mActivity);
		this.mRecords = DBStorage.getMarkRecords(mActivity);
	}
	
	@Override
	public int getCount() {
		return mRecords.size();
	}

	@Override
	public Object getItem(int position) {
		return mRecords.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = mInflater.inflate(com.sc.lane.R.layout.main_fragement_photos_item, null);
		
		GridView gridview = (GridView) convertView.findViewById(R.id.gv_photos);
		TextView mtvTitle = (TextView) convertView.findViewById(R.id.tv_title);
		MarkRecord record = mRecords.get(position);
		String title = "未命名的一次行程";
		if(record.getDescription() != null){
			title = record.getDescription();
		}
		mtvTitle.setText(title);

		gridview.setAdapter(new GridPhotoAdapter(mActivity, record.getId()));
		
		return convertView;
	}

}
